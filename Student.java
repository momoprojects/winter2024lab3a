public class Student{
	String schoolName;
	int age;
	String favColor;
	
	public Student(String studentSchoolName, int studentAge, String studentfavColor){
		schoolName= studentSchoolName;
		age= studentAge;
		favColor= studentfavColor;
	}
	
	public String greetFromSchool(){
		return "hello! I come from "+schoolName+".";
	}
	public String ColorOfCar(){
		return favColor;
	}
}