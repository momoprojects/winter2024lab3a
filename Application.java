public class Application{
	public static void main(String[] args){
		Student student1= new Student("Lucille", 30, "yellow");
		Student student2= new Student("Dawson", 16, "purple");
		
		Student[] section3= new Student[3];
		section3[0]= student1;
		section3[1]=student2;
		section3[2]= new Student("Mars", 99, "light green");
		System.out.println(section3[2].age);

	}
}